package com.example.manos.smartmuseums;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.VideoView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.Locale;

/**
 * Questa classe rispecchia l'item funzionale 2.2.2.1 nonchè 2.2.2.2 nonchè 2.2.2.3
 */
public class ActivityMuseum extends Activity implements TextToSpeech.OnInitListener {

    private VideoView vd;
    private EditText tv;
    /**
     * Questo oggetto gestisce la connessione a database attraverso una pagina PHP
     */
    RequestQueue requestQueue;
    private final String url = "http://durresarchmuseum.altervista.org";
    private String idOpera;
    /**
     * Questo pulsante gestisce la sintetizzazione vocale
     */
    Button tts;
    /**
     * Questo pulsante gestisce la messa in pausa di un video
     */
    Button pauseV;
    /**
     * Questo pulsante gestisce la ripresa e/o il primo avvio di un video
     */
    Button playV;
    private TextToSpeech textToSpeech;
    private String speak;

    /**
     * Questo metodo finalizza l'activity permettendo la distruzione di tutti gli oggetti
     */
    @Override
    public void onDestroy() {
        if (textToSpeech.isSpeaking()) {
            textToSpeech.stop();
        }
        if (vd.isPlaying()) {
            vd.pause();
        }
        super.onDestroy();
    }

    /**
     * Metodo di default per l'istanziazione di una activity
     *
     * @param savedInstanceState un Bundle implicito
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum);
        Intent intent = getIntent();
        idOpera = intent.getStringExtra("id");
        tv = (EditText) findViewById(R.id.tv);
        tv.setFocusable(false);
        tv.setClickable(true);
        tts = (Button) findViewById(R.id.invokeTTS);
        tts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textToSpeech.isSpeaking()) {
                    textToSpeech.stop();
                    tts.setText("START TTS!");
                } else {
                    textToSpeech.speak(speak, TextToSpeech.QUEUE_FLUSH, null);
                    tts.setText("STOP TTS!");
                }
            }
        });
        goOn();
    }

    /**
     * Questo metodo esegue la funzionalità principale della activity, ossia la visualizzazione della scheda reperto
     */
    public void goOn() {
        requestQueue = Volley.newRequestQueue(this);
        Log.i("Into url", "appena prima di ON RESPONSE");
        final String spezzone = "/connApp.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                url + spezzone, new Response.Listener<JSONObject>() {
            /**
             * Questo metodo gestisce l'avvenuta richiesta di connessione con la pagina Web
             * @param response una risposta in formato JSON
             */
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String id = "", nome = "", anno = "", autore = "", materiale = "", categoria = "", urlVideo = "", urlAudio="", tecnica = "", periodoStorico = "", dimensioni = "", peso = "", valore = "", infoStoriche = "", descrizione = "";
                    Log.i("Into onResponse", "secondo try");
                    JSONArray arrayOpere = response.getJSONArray("opere");//TODO il nome della variabile php deve essere "opere"
                    final int opereL = arrayOpere.length();
                    for (int i = 0; i < opereL; i++) {
                        JSONArray arrayOpera = arrayOpere.optJSONArray(i);
                        final int operaL = arrayOpera.length();
                        for (int j = 0; j < operaL; j++) {
                            JSONObject opera = arrayOpera.optJSONObject(j);
                            id = opera.getString("id");
                            nome = opera.getString("nome");
                            anno = opera.getString("anno");
                            autore = opera.getString("autore");
                            materiale = opera.getString("materiale");
                            categoria = opera.getString("categoria");
                            urlVideo = opera.getString("urlVideo");
                            urlAudio = opera.getString("urlAudio");
                            tecnica = opera.getString("tecnica");
                            periodoStorico = opera.getString("periodoStorico");
                            dimensioni = opera.getString("dimensioni");
                            peso = opera.getString("peso");
                            valore = opera.getString("valore");
                            infoStoriche = opera.getString("infoStoriche");
                            descrizione = opera.getString("descrizione");
                            if (id.equals(idOpera)) {
                                speak = "Quest'opera, nota come " + nome + ", creata da " + autore + " nell'anno " + anno + ", è fatta di " +
                                        materiale + " e risale alla categoria " + categoria + "; fu inoltre realizzata nel periodo storico noto " +
                                        "come " + periodoStorico + " secondo la tecnica detta " + tecnica + ". " +
                                        "L'opera, avente dimensioni " + dimensioni + ", peso pari a circa " + peso + " chili, ha un valore stimato di " + valore + " euro. " +
                                        "Ora alcune info storiche: " + infoStoriche + ". " +
                                        "E, per concludere, una descrizione. " + descrizione + ".";
                                MediaPlayer player=new MediaPlayer();
                                player.setDataSource(url+"/"+urlAudio);
                                player.prepare();
                                player.start();
                                tv.setText(speak);
                                pauseV = (Button) findViewById(R.id.pauseButton);
                                playV = (Button) findViewById(R.id.playButton);
                                vd = (VideoView) findViewById(R.id.video);
                                MediaController mediaController = new MediaController(getApplicationContext());
                                mediaController.setAnchorView(vd);
                                mediaController.setMediaPlayer(vd);
                                vd.setVideoURI(Uri.parse(url + "/" + urlVideo.trim()));
                                vd.setMediaController(mediaController);
                                vd.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View view, MotionEvent motionEvent) {
                                        return true;
                                    }
                                });
                                playV.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        vd.start();
                                    }
                                });
                                pauseV.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        vd.pause();
                                    }
                                });
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.i("RequestQueue", e.getMessage());
                } catch (IOException ioe) {
                    Log.e("IOException", ioe.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Into onErrorResponse", "fittizio");

            }
        });
        requestQueue.add(jsonObjectRequest);
        textToSpeech = new TextToSpeech(this, this);
    }

    /**
     * Questo metodo effettua i preparativi per il TTS
     *
     * @param i un valore implicito della classe TextToSpeech che serve per gestire la voce
     */
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            int lang = textToSpeech.setLanguage(Locale.ITALIAN);
            if (lang == TextToSpeech.LANG_NOT_SUPPORTED || lang == TextToSpeech.LANG_MISSING_DATA) {
                textToSpeech.speak(speak, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }
}