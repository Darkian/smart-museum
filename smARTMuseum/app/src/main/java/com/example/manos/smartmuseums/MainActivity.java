package com.example.manos.smartmuseums;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Questa è la classe principale e premette all'utente di scannerizzare il QR-Code.
 * Soddisfa l'IF 2.2.2.1
 */
public class MainActivity extends Activity implements ZXingScannerView.ResultHandler{

    private final int requestCodePermission =2;
    private final String[] mPermission = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Button scan;
    private ZXingScannerView mScannerView;

    /**
     * Questo metodo avvia il motore di sintetizzazione vocale
     * @param savedInstanceState stato di istanza
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(Build.VERSION.SDK_INT>=23) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), mPermission[0]) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), mPermission[1]) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), mPermission[2]) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(getApplicationContext(), mPermission[3]) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, mPermission, requestCodePermission);
            }
        } else {
            ActivityCompat.requestPermissions(this, mPermission, requestCodePermission);
        }
        scan = (Button) findViewById(R.id.button);
    }

    /**
     * Questo metodo gestisce il caso in cui le permissions siano state date correttamente
     * @param requestCode numero intero maggiore di zero
     * @param permissions quali sono le permissions da controllare
     * @param grantResults codice relativo a ciascuna permission
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCodePermission == requestCode) {
            if(grantResults.length==5 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[3] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    /**
     * Questo metodo è un listener per il pulsante di scansione del codice QR: appena premuto, lancia tale operazione
     * @param v vista di origine
     */
    public void onClick(View v) {
        mScannerView=new ZXingScannerView(this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    /**
     * Questo metodo stoppa la scansione
     */
    @Override
    public void onPause() {
        if(mScannerView!=null) {
            mScannerView.stopCamera();
        }
        super.onPause();
    }

    /**
     * Questo metodo gestisce cosa accade quando il QR-Code è letto con successo
     * @param result tipo grezzo di risultato
     */
    @Override
    public void handleResult(Result result) {
        onPause();
        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Scan Result");
        final int startTrimming=5;
        final String s=result.getText().toString().substring(startTrimming, result.getText().toString().length()).trim();
        builder.setMessage("Scannerizzato con successo!");
        builder.setPositiveButton("PROSEGUI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onPause();
                Intent intent = new Intent(getApplicationContext(), ActivityMuseum.class);//passa alla parte di recupero dati dell'opera
                intent.putExtra("id", s);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("ANNULLA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);//annulla tutto
                startActivity(intent);
            }
        });
        AlertDialog alert1 = builder.create();
        alert1.show();
    }
}