-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Lug 06, 2017 alle 08:54
-- Versione del server: 5.7.14
-- Versione PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `museo_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `evento`
--

CREATE TABLE `evento` (
  `id` int(10) NOT NULL,
  `data` date NOT NULL,
  `titolo` varchar(45) NOT NULL,
  `descrizione` varchar(2000) DEFAULT NULL,
  `sede_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `evento`
--

INSERT INTO `evento` (`id`, `data`, `titolo`, `descrizione`, `sede_id`) VALUES
(1, '2017-07-13', 'Mostra romana', 'lkjblkjbjkbk', 3),
(3, '2017-09-25', 'prova evento', 'lalalalaaaaaaaaaa', 3),
(2, '2017-07-30', 'Escursione a Butrinto', 'Un tuffo nel passato per riscoprire usi e costumi della civiltÃ  greca e romana', 1),
(4, '2017-09-25', 'prova2', 'lalalalaaaaaaaaaa', 3),
(5, '2017-09-14', 'prova sto cazzo', 'sto cazzzooooooooooo', 1),
(6, '2017-10-26', 'Conferenza su skype', 'ciao ciao ciao ciao', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `opera`
--

CREATE TABLE `opera` (
  `id` int(10) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `anno` varchar(10) DEFAULT NULL,
  `autore` varchar(30) DEFAULT NULL,
  `proprietario` varchar(30) NOT NULL,
  `numPass` char(9) DEFAULT NULL,
  `location` varchar(25) DEFAULT NULL,
  `materiale` varchar(30) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `urlFoto` varchar(100) DEFAULT NULL,
  `urlVideo` varchar(500) DEFAULT NULL,
  `dataRicezione` date DEFAULT NULL,
  `tecnica` varchar(45) NOT NULL,
  `periodoStorico` varchar(30) DEFAULT NULL,
  `dimensioni` varchar(30) DEFAULT NULL,
  `peso` float NOT NULL,
  `valore` double NOT NULL,
  `dittaConsegna` varchar(45) DEFAULT NULL,
  `integrita` enum('0','1','2','3','4','5','6','7','8','9','10') NOT NULL,
  `original` enum('true','false') NOT NULL,
  `luogoOrigine` varchar(30) DEFAULT NULL,
  `infoStoriche` varchar(2000) DEFAULT NULL,
  `descrizione` varchar(2000) DEFAULT NULL,
  `restauriEffettuati` varchar(2000) DEFAULT NULL,
  `dataPubblicazione` date NOT NULL,
  `sede_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `opera`
--

INSERT INTO `opera` (`id`, `nome`, `anno`, `autore`, `proprietario`, `numPass`, `location`, `materiale`, `categoria`, `urlFoto`, `urlVideo`, `dataRicezione`, `tecnica`, `periodoStorico`, `dimensioni`, `peso`, `valore`, `dittaConsegna`, `integrita`, `original`, `luogoOrigine`, `infoStoriche`, `descrizione`, `restauriEffettuati`, `dataPubblicazione`, `sede_id`) VALUES
(1, 'Vaso di Achille e Aiace', '200 a.c.', 'Pandora', 'Francesco srl', 'KK0000000', 'Foggia', 'Argilla', 'Vasi', NULL, '<iframe width="560" height="315" src="https://www.youtube.com/embed/9HVHKRVlSU4" frameborder="0" allowfullscreen></iframe>', '2017-06-15', 'ceramica', 'periodo ellenico', '100cm x 50cm x 50cm', 7, 5000000, NULL, '6', 'true', 'Creta', 'ne ha passate tante', 'rappresenta Achille e Aiace che giocano a dadi', 'Ripristino pigmento nero', '2017-06-30', 1),
(2, 'Moschophoros', '570a.c', NULL, 'vitoPepe spa', NULL, 'Putignano', 'marmo', 'Scultura', NULL, NULL, '2017-05-07', 'Scultura', 'età greca arcaica', '162cm ', 40, 2000000, 'Bartolini', '4', 'false', 'Atene', 'Anche questa ne ha passate tante', 'In questa scultura viene rappresentato un uomo, probabilmente lo stesso offerente, che porta sulle spalle un vitello, secondo uno schema noto sin dal Kriophoros cretese del VII secolo a.C. ora a Berlino. Ai piedi di questa scultura si legge: "Rhombos, figlio di Palos ha dedicato". L\'occasione della dedica fatta ad Atena è incerta: potrebbe trattarsi del vincitore di una gara che aveva come premio un vitello o di un sacrificio in onore della dea.', 'Non è stato effettuato ancora alcun restauro', '2017-06-21', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `operatoremuseo`
--

CREATE TABLE `operatoremuseo` (
  `username` varchar(30) NOT NULL,
  `password` varchar(70) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `cf` char(16) DEFAULT NULL,
  `dataNascita` date NOT NULL,
  `ammministratore` enum('true','false') NOT NULL,
  `citta` varchar(30) DEFAULT NULL,
  `sede_id` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `operatoremuseo`
--

INSERT INTO `operatoremuseo` (`username`, `password`, `email`, `telefono`, `nome`, `cognome`, `cf`, `dataNascita`, `ammministratore`, `citta`, `sede_id`) VALUES
('p.pastore', 'ciao', 'asdasda@adad.com', '1234567890', 'Paolo', 'Pastore', NULL, '1995-02-20', 'true', 'Durazzo', 2),
('v.pepe', 'mondo', 'padns@sada.it', '9876543210', 'Vito', 'Pepe', 'QWERTYUIOP789AS5', '1994-08-28', 'false', 'Valona', 1),
('l.manosperta', 'RoHMli05Oy', 'paolo.pastore.95@gmail.com', '3286470802', 'Luigi', 'Manosperta', 'ASDAD468135DSAWI', '1996-07-04', 'true', 'Bari', 1),
('d.monaco', '6im4TALv5z', 'daniele.monaco@gmail.com', '3256325663', 'Daniele', 'Monaco', '', '1996-03-14', 'true', 'Bari', 1),
('g.pasquali', 'ApgkNMU5ao', 'provaprova@ingsoft.it', '9632587411', 'Giuseppe', 'Pasquali', '', '1993-11-25', 'false', 'Molfetta', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `sede`
--

CREATE TABLE `sede` (
  `id` int(10) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `indirizzo` varchar(50) NOT NULL,
  `numTelefono` varchar(13) NOT NULL,
  `citta` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `sede`
--

INSERT INTO `sede` (`id`, `nome`, `indirizzo`, `numTelefono`, `citta`) VALUES
(1, 'Durazzo', 'via palma 5', '0123456789', '2'),
(3, 'prova', 'via imperiale 13', '1597534560', '2');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `opera`
--
ALTER TABLE `opera`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `operatoremuseo`
--
ALTER TABLE `operatoremuseo`
  ADD PRIMARY KEY (`username`);

--
-- Indici per le tabelle `sede`
--
ALTER TABLE `sede`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `evento`
--
ALTER TABLE `evento`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la tabella `opera`
--
ALTER TABLE `opera`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `sede`
--
ALTER TABLE `sede`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
