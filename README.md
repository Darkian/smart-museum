This is a group project done during semester 2, 2nd year of my Bachelor's degree; the module was "Software Engineering".
The objective was to improve the hedonic experience connected to going to the museum through low-cost multimedia guide, in the form of an Android app which is able to download the artwork's details through a QRCode.
This case study represents a real need of the Durazzo Museum in Albany, the University of Bari "Aldo Moro" and the Italian "Distretto Produttivo dell’Informatica Pugliese".
There were 2 macro-requirements: server component (which acts as a means by which the user can interact with the Museum artwork collection); mobile component (which is able to retrieve the single artwork's details by scanning a QRCode put near the artwork itself).
The server component must be able to add, modify, delete: artworks, museums locations, audio-files related to single artworks; as well as this, the server must be able to publish an artwork, to generate&print a QRCode, and to manage the museum's staff accounts.
The mobile component must be able to scan a QRCode, display the artwork's info, and to play an audio-guide if the user wants to.
The entire case study must be developed using SCRUM User&Quality Oriented agile technique. The development platform must include: the Android Studio IDE, SVN, GRADLE, Redmine, SCRUM plugin on it, Kiwuan. Th app version must be 4.x or higher. SOlutions must met the ISO25010 standard and Kiwuan overall index must be greater or equal to 70%.
---------------------------------------------------------------------
